from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from chrome_driver.chrome_driver import ChromeDriver
import urllib
import binascii


class AuthorList:
    URL = "https://scholar.google.com/citations?hl=en&view_op=search_authors&mauthors="

    def __init__(self, uni_domain):
        self.first_page_url = self.URL + uni_domain + "&btnG="

    def filtered(self, element, word):
        try:
            element = element.split(word)[1]
            return element
        except Exception as e:
            element = 0
            return element

    def check_next_page(self, chrome_browser):


            if chrome_browser.find_elements_by_xpath(
                    "//button[@class='gs_btnPR gs_in_ib gs_btn_half gs_btn_lsb gs_btn_srt "
                    "gsc_pgn_pnx']"):
                try:
                    next_button = chrome_browser.find_elements_by_xpath("//button[@class='gs_btnPR gs_in_ib gs_btn_half gs_btn_lsb "
                                                          "gs_btn_srt gsc_pgn_pnx']")[0]
                    if next_button.get_attribute("onclick"):
                        next_page_url = next_button.get_attribute("onClick")
                        url_key = next_page_url[17:-1].replace("\\/", "/").encode().decode('unicode_escape')
                        next_page_url = "https://scholar.google.com" + url_key
                        print("Next Page Url", next_page_url)

                        chrome_browser.set_page_load_timeout(30)

                        return next_page_url

                    else:
                        print("no next page found !")
                        chrome_browser.quit()
                        return False

                except Exception as e:
                    print(e)
                    print("exception in main while loop")
                    return False
            else:
                return False


    def extract_details(self, chrome_browser, page_url):
        researcher_data = []
        try:

            links = WebDriverWait(chrome_browser, 30).until(
                EC.visibility_of_all_elements_located((By.CSS_SELECTOR, '.gs_ai_name a')))
            name = chrome_browser.find_elements_by_xpath("//h3[@class='gs_ai_name']")
            emails = chrome_browser.find_elements_by_xpath("//div[@class='gs_ai_eml']")
            citations = chrome_browser.find_elements_by_xpath("//div[@class='gs_ai_cby']")
            field_tags = chrome_browser.find_elements_by_xpath("//div[@class='gs_ai_int']")

            for l, e, c, ft in zip(links, emails, citations, field_tags):
                name = l.text
                email = self.filtered(e.text, 'at')
                citation = self.filtered(c.text, 'by')
                profile_link = l.get_attribute('href')
                field_tag = ft.text
                row = [name, email, citation, profile_link, field_tag]
                researcher_data.append(row)

            return researcher_data
        except TimeoutException as ex:
            print("something wrong in extraction", ex)
            return researcher_data

        except WebDriverException as ex:
            return researcher_data
