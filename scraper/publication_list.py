from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from chrome_driver.chrome_driver import ChromeDriver


class PublicationList:

    def __init__(self, scholar_url):
        self.scholar_url = scholar_url


    def get_author_publications(self):
        publication_list = []
        self.chrome_browser.get(self.scholar_url)
        publications = self.chrome_browser.find_elements_by_xpath("//a[@class='gsc_a_at']")
        for paper in publications:
            paper = paper.get_attribute('href')
            publication_list.append(paper)
        return publication_list

    def get_paper_remote_links(self):
        publications = self.get_author_publications()
        paper_links = []
        for scholar_link in publications:
            self.chrome_browser.get(scholar_link)
            paper_host_link = self.chrome_browser.find_element_by_xpath("//a[@class='gsc_oci_title_link']")
            paper_link = paper_host_link.get_attribute('href')
            paper_links.append(paper_link)

        return paper_link



'''
s = PublicationList("RselZR8AAAAJ")
s.get_publication_links()
'''