from util import internal_utils as itu
import pycountry_convert as pc
from bs4 import BeautifulSoup
import os, inspect, re
import pandas as pd
import requests
from pathlib import Path


class University:
    TOP_RANKING_UNI_URL = "https://www.webometrics.info/en/"
    TOP_RANKING_HEADER = ['Rank', 'Domain', 'University']
    FILE_EXTENSION = "-top-uni.csv"
    uni_domain_list = list()

    def __init__(self, country, top_ranked_limit):

        self.country = country
        self.top_ranked_limit = top_ranked_limit
        self.countries_csv_dir = str(Path(inspect.getfile(self.__class__)).absolute().parent.parent.parent) + str(
            os.sep) + "util" + str(os.sep) + 'countries.csv'
        self.continent = self.country_to_continent(self.country)
        self.file_dir_name = str(Path(inspect.getfile(self.__class__)).absolute().parent.parent.parent) + str(
            os.sep) + "scholar_data" + str(os.sep) + self.country + str(
            os.sep) + self.country + self.FILE_EXTENSION

    def country_to_continent(self, country_name):
        try:
            print(self.countries_csv_dir)
            country_df = pd.read_csv(self.countries_csv_dir)
            for index, row in country_df.iterrows():
                country = row['countries'].title()
                country_continent_name = row['continent'].title()
                if country.strip() == country_name.title().strip():
                    return country_continent_name.strip()

        except Exception as e:
            print("country continent is invalid or missing.")
            return country_name

    def prepare_url(self):
        webometrics_url = self.TOP_RANKING_UNI_URL + str(self.continent) + "/" + self.country
        print("webometrics url ==> ", webometrics_url)
        return webometrics_url

    def get_page_code(self):
        r = requests.get(self.prepare_url())
        soup = BeautifulSoup(r.text, 'lxml')
        table = soup.find('table', {'class': 'sticky-enabled'})
        a_tags = table.find_all('a', text=True, attrs={'href': re.compile("^https|http://")})
        return a_tags

    def filter_uni_domain(self, domain):
        if domain.endswith('/'):
            domain = domain[:-1]
        if domain.startswith('https://'):
            domain = domain[8:]
        if domain.startswith('http://'):
            domain = domain[7:]
        if domain.startswith('www'):
            domain = domain[3:]
        if domain.startswith('.'):
            domain = domain[1:]
        return domain

    def extract_uni_data(self):
        a_tags = self.get_page_code()
        uni_data = list()
        for a_tag in a_tags:
            if len(uni_data) == self.top_ranked_limit:
                break

            uni_link = self.filter_uni_domain(a_tag.get('href'))
            uni_name = a_tag.text
            uni_data.append([len(uni_data), uni_link, uni_name])
            self.uni_domain_list.append(uni_link)

        top_uni_df = pd.DataFrame(uni_data, columns=self.TOP_RANKING_HEADER)
        top_uni_df.to_csv(self.file_dir_name, index=False)
        return uni_data

    def get_top_uni_domains(self):
        self.extract_uni_data()
        return self.uni_domain_list


'''
u = University("RUSSIAN FEDERATION",35)
print((u.get_top_uni_domains()))
'''
