import os
import inspect
import pandas as pd
from pathlib import Path
import time
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from scraper.publication_list import PublicationList
from chrome_driver.chrome_driver import ChromeDriver
from selenium import webdriver
from proxy.proxy import Proxy


class Publication:
    FILE_EXTENSION = ".csv"
    RESEARCHER_FILE_NAME = "-researcher" + FILE_EXTENSION
    PUBLICATION_FILE_NAME = "-publications" + FILE_EXTENSION

    def __init__(self, country):
        self.country = country
        self.initial_path = str(Path(inspect.getfile(self.__class__)).absolute().parent.parent.parent)
        self.dir_path = self.initial_path + str(os.sep) + "scholar_data" + str(os.sep) + self.country + str(os.sep)
        self.researcher_data_dir = self.dir_path + self.country + self.RESEARCHER_FILE_NAME
        self.paper_links_df = self.create_publication_df()

    def create_publication_df(self):
        if os.path.isfile(self.dir_path + self.country + self.PUBLICATION_FILE_NAME) == False:
            df = pd.DataFrame(columns=['researcher', 'paper links'])
            df.to_csv(self.dir_path + self.country + self.PUBLICATION_FILE_NAME, index=False)
            return df
        else:
            df = pd.read_csv(self.dir_path + self.country + self.PUBLICATION_FILE_NAME)
            return df

    def read_researcher_file(self):
        researcher_df = pd.read_csv(self.researcher_data_dir)
        return researcher_df

    def append_publication_data(self, new_data_row):
        self.paper_links_df.loc[len(self.paper_links_df)] = new_data_row
        self.paper_links_df.to_csv(self.dir_path + self.country + self.PUBLICATION_FILE_NAME, mode='a',
                                   header=False, index=False)
        self.paper_links_df = pd.DataFrame(columns=self.paper_links_df.columns)

    def get_previous_progress(self):
        if os.path.isfile(self.dir_path + self.country + "-reference-link-progress.csv"):
            progress_df = pd.read_csv(self.dir_path + self.country + "-reference-link-progress.csv")
            return progress_df
        else:
            progress_df = pd.DataFrame(columns=['last'])
            return progress_df

    def extract_publications(self):
        progress_df = self.get_previous_progress()
        researcher_df = self.read_researcher_file()
        driver = ChromeDriver()
        driver_dir = driver.get_chrome_driver_dir()
        new_proxy = Proxy()
        proxy = new_proxy.get_proxies()
        proxy_ip = proxy[0]
        options = driver.prepare_options()
        print("latest proxy", proxy_ip)
        options.add_argument('--proxy-server={}'.format(proxy_ip))
        chrome_browser = webdriver.Chrome(options=options, executable_path=driver_dir)
        error = False
        count = 0
        if len(progress_df.index) > 0:

            last_index = progress_df['last'].iloc[0]
            print(last_index)
        else:
            last_index = 0
        print("last progress", last_index)

        for index, row in researcher_df.iloc[last_index:].iterrows():
            try:
                profile_link = row['profile']
                next_page = profile_link
                chrome_browser.get(profile_link)

                if 'sorry' in chrome_browser.current_url:
                    print("bot detection url.........")
                    driver.add_expired_driver_proxy()
                    error = True

                if 'Sorry' in chrome_browser.title:
                    print("bot detection url.........")
                    driver.add_expired_driver_proxy()
                    error = True

                while error:
                    chrome_browser.quit()
                    driver_dir = driver.get_chrome_driver_dir()
                    proxy = new_proxy.get_proxies()
                    proxy_ip = proxy[0]
                    print("latest proxy", proxy_ip)
                    options = driver.prepare_options()
                    options.add_argument('--proxy-server={}'.format(proxy_ip))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=driver_dir)
                    if driver.validate_proxy_address(chrome_browser, next_page):
                        print("proxy working correctly")
                        error = False
                    else:
                        print("re-instantiating the driver")

                if not error:
                    WebDriverWait(chrome_browser, 10).until(EC.title_contains("Google Scholar"))
                    publications = chrome_browser.find_elements_by_xpath("//a[@class='gsc_a_at']")
                    links = [elem.get_attribute('href') for elem in publications]
                    for link in links:
                        profile_id = profile_link.split('=')[2]
                        data = [profile_id, link]
                        self.append_publication_data(data)
                    print(last_index, " reference links extracted ", profile_link, str(len(links)))
                    progress_df = pd.DataFrame(columns=['last'])
                    progress_df.loc[len(progress_df)] = last_index
                    progress_df.to_csv(self.dir_path + self.country + "-reference-link-progress.csv", index=False)
                    last_index = last_index + 1
                    error = False


            except TimeoutException as ex:
                print("Time out exeception", ex)
                driver.add_expired_driver_proxy()
                error = True

            except WebDriverException as ex:
                print("Web Driver", ex)
                time.sleep(5)
                driver.add_expired_driver_proxy()
                error = True


