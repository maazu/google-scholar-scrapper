import os
import inspect
import pandas as pd
from pathlib import Path
import time
from selenium.common.exceptions import TimeoutException, WebDriverException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from scraper.publication_list import PublicationList
from chrome_driver.chrome_driver import ChromeDriver
from selenium import webdriver
from proxy.proxy import Proxy
from selenium.webdriver.common.by import By
import traceback

class PaperRemoteLink:

    FILE_EXTENSION = ".csv"
    PUBLICATION_FILE_NAME = "-publications" + FILE_EXTENSION
    REMOTE_FILE_NAME = "-paper-host-links" + FILE_EXTENSION

    def __init__(self, country):
        self.country = country
        self.initial_path = str(Path(inspect.getfile(self.__class__)).absolute().parent.parent.parent)
        self.dir_path = self.initial_path + str(os.sep) + "scholar_data" + str(os.sep) + self.country + str(os.sep)
        self.publication_data_dir = self.dir_path + self.country + self.PUBLICATION_FILE_NAME
        self.paper_remote_links_df = self.create_host_link_df()

    def create_host_link_df(self):
        if not os.path.isfile(self.dir_path + self.country + self.REMOTE_FILE_NAME):
            print("Creating Remote link File...")
            df = pd.DataFrame(columns=['researcher', 'reference link', 'host link'])
            df.to_csv(self.dir_path + self.country + self.REMOTE_FILE_NAME, index=False)
            print("Remote link File Created...", self.dir_path + self.country + self.REMOTE_FILE_NAME)
            return df
        else:
            print("Previous Remote link File loaded..\nFile : ", self.dir_path + self.country + self.REMOTE_FILE_NAME)
            df = pd.read_csv(self.dir_path + self.country + self.REMOTE_FILE_NAME)
            return df

    def read_publication_file(self):
        publication_df = pd.read_csv(self.publication_data_dir)
        return publication_df

    def append_remote_link_data(self, new_data_row):
        self.paper_remote_links_df.loc[len(self.paper_remote_links_df)] = new_data_row
        self.paper_remote_links_df.to_csv(self.dir_path + self.country + self.REMOTE_FILE_NAME, mode='a',
                                          header=False, index=False)
        self.paper_remote_links_df = pd.DataFrame(columns=self.paper_remote_links_df.columns)

    def get_previous_progress(self):
        if os.path.isfile(self.dir_path + self.country + "-paper-host-links-progress.csv"):
            progress_df = pd.read_csv(self.dir_path + self.country + "-paper-host-links-progress.csv")
            return progress_df
        else:
            progress_df = pd.DataFrame(columns=['last'])
            return progress_df

    def extract_remote_link(self):
        progress_df = self.get_previous_progress()
        publication_df = self.read_publication_file()
        driver = ChromeDriver()
        driver_dir = driver.get_chrome_driver_dir()
        new_proxy = Proxy()
        proxy = new_proxy.get_proxies()
        proxy_ip = proxy[0]
        options = driver.prepare_options()
        print("latest proxy", proxy_ip)
        options.add_argument('--proxy-server={}'.format(proxy_ip))
        chrome_browser = webdriver.Chrome(options=options, executable_path=driver_dir)
        error = False
        count = 0
        if len(progress_df.index) > 0:

            last_index = progress_df['last'].iloc[0]
            print(last_index)
        else:
            last_index = 0
        print("last progress for remote links ==> ", last_index)

        for index, row in publication_df.iloc[last_index:].iterrows():
            try:
                profile_link = row['researcher']
                publication_reference_link = row['paper links']
                next_page = publication_reference_link
                chrome_browser.get(publication_reference_link)

                if 'sorry' in chrome_browser.current_url:
                    print("bot detection url.........")
                    driver.add_expired_driver_proxy()
                    error = True

                if 'Sorry' in chrome_browser.title:
                    print("bot detection url.........")
                    driver.add_expired_driver_proxy()
                    error = True

                while error:
                    driver.add_expired_driver_proxy()
                    chrome_browser.quit()
                    driver_dir = driver.get_chrome_driver_dir()
                    proxy = new_proxy.get_proxies()
                    proxy_ip = proxy[0]
                    print("Resolving issue with latest new proxy address: ", proxy_ip)
                    options = driver.prepare_options()
                    options.add_argument('--proxy-server={}'.format(proxy_ip))
                    chrome_browser = webdriver.Chrome(options=options, executable_path=driver_dir)
                    if driver.validate_proxy_address(chrome_browser, next_page):
                        print("proxy working correctly")
                        error = False
                    else:
                        print("re-instantiating the driver")

                if not error:
                    try:
                        WebDriverWait(chrome_browser, 10).until(EC.title_contains("View article"))
                        WebDriverWait(chrome_browser, 10).until(EC.visibility_of_element_located((By.ID, "gsc_oci_title")))
                        paper_host_link_a_tag = chrome_browser.find_element_by_xpath("//a[@class='gsc_oci_title_link']")
                        paper_host_link = paper_host_link_a_tag.get_attribute('href')
                        data = [profile_link,publication_reference_link,paper_host_link]
                        self.append_remote_link_data(data)
                        print(last_index, " remote links extracted ", profile_link, paper_host_link)
                        progress_df = pd.DataFrame(columns=['last'])
                        progress_df.loc[len(progress_df)] = last_index
                        progress_df.to_csv(self.dir_path + self.country + "-paper-host-links-progress.csv", index=False)
                        last_index = last_index + 1
                        error = False
                    except NoSuchElementException as e:
                        print("escaping the reference link " + publication_reference_link + "\nremote link not available")


            except TimeoutException as ex:
                print("Time out exeception", ex)
                error = True

            except WebDriverException as ex:
                print("Web Driver Issue ", ex)
                traceback.print_exc()
                time.sleep(2)

                error = True

