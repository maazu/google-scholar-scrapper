import inspect
import os
from pathlib import Path
from selenium import webdriver
import pandas as pd
from selenium.common.exceptions import TimeoutException, WebDriverException
from chrome_driver.chrome_driver import ChromeDriver
from component.universites.university import University
from scraper.author_list import AuthorList
from proxy.proxy import Proxy
import traceback


class Researcher:
    PROGRESS_FILE_NAME = "-researcher-extraction-progress"
    RESEARCHER_FILE_NAME = "-researcher"
    FILE_EXTENSION = ".csv"

    def __init__(self, country, top_rank_limit):
        self.country = country
        self.file_dir_name = str(Path(inspect.getfile(self.__class__)).absolute().parent.parent.parent) + str(
            os.sep) + "scholar_data" + str(os.sep) + self.country + str(
            os.sep) + self.country
        self.scholar_df = self.create_researcher_df()
        self.top_rank_limit = top_rank_limit
        self.progress_df = self.get_previous_progress()

    def get_top_uni_domain(self):
        uni_domain = University(self.country, self.top_rank_limit)
        uni_domains = uni_domain.get_top_uni_domains()
        print("Prepared the uni_domain", len(uni_domains))
        return uni_domains

    def create_researcher_df(self):
        if not os.path.isfile(self.file_dir_name + self.RESEARCHER_FILE_NAME + self.FILE_EXTENSION):
            print("Creating Researchers Data File...")
            df = pd.DataFrame(columns=['Name', 'email', 'citations', 'profile', 'Field Tags'])
            df.to_csv(self.file_dir_name + self.RESEARCHER_FILE_NAME + self.FILE_EXTENSION, index=False)
            print("Researchers Data File Created...", self.file_dir_name + self.RESEARCHER_FILE_NAME + self.FILE_EXTENSION)
            return df
        else:
            print("Previous Researchers  File loaded..\nFile : " + self.file_dir_name + self.RESEARCHER_FILE_NAME + self.FILE_EXTENSION)
            df = pd.read_csv(self.file_dir_name + self.RESEARCHER_FILE_NAME + self.FILE_EXTENSION)
            return df

    def get_previous_progress(self):
        print("loading from the previous progress")
        if os.path.isfile(self.file_dir_name + self.PROGRESS_FILE_NAME + self.FILE_EXTENSION):
            progress_df = pd.read_csv(self.file_dir_name + self.PROGRESS_FILE_NAME + self.FILE_EXTENSION)
            return progress_df
        else:
            print("creating the new progress")
            progress_df = pd.DataFrame(columns=['uni', 'current url', 'previous url'])
            progress_df.to_csv(self.file_dir_name + self.PROGRESS_FILE_NAME + self.FILE_EXTENSION, index=False)
            return progress_df

    def append_progress_data(self, new_data_row):
        self.progress_df.loc[len(self.progress_df)] = new_data_row
        self.progress_df.to_csv(self.file_dir_name + self.PROGRESS_FILE_NAME + self.FILE_EXTENSION, mode='a',
                                header=False, index=False)
        self.progress_df = pd.DataFrame(columns=self.progress_df.columns)
        print("progress appended in the csv ")

    def append_researcher_data(self, new_data_row):

        for row in new_data_row:
            self.scholar_df.loc[len(self.scholar_df)] = row
            self.scholar_df.to_csv(self.file_dir_name + self.RESEARCHER_FILE_NAME + self.FILE_EXTENSION, mode='a',
                                   header=False, index=False)
            self.scholar_df = pd.DataFrame(columns=self.scholar_df.columns)
            print(row)

    def flip_pages(self, org_domain, initial_url):
        driver = ChromeDriver()
        new_proxy = Proxy()
        active_page = ['']
        active_page[0] = initial_url
        next_page = initial_url
        error = True
        while True:
            try:
                if error:
                    while error:
                        try:
                            driver_dir = driver.get_chrome_driver_dir()
                            options = driver.prepare_options()
                            proxy = new_proxy.get_proxies()
                            proxy_ip = proxy[0]
                            options.add_argument('--proxy-server={}'.format(proxy_ip))
                            print("latest proxy", proxy_ip)
                            chrome_browser = webdriver.Chrome(options=options, executable_path=driver_dir)
                            if driver.validate_proxy_address(chrome_browser, next_page):
                                print("proxy working correctly")
                                error = False
                            else:
                                print("re-instantiating the driver")
                                chrome_browser.quit()

                        except Exception as e:
                            print("something went while resolving bot error")
                            print(e)
                else:
                    try:
                        print(chrome_browser)
                        chrome_browser.get(next_page)
                        auth = AuthorList(org_domain)
                        researcher_data = auth.extract_details(chrome_browser, next_page)

                        if 'sorry' in chrome_browser.current_url:
                            print("Bot Protection Url detected...")
                            driver.add_expired_driver_proxy()
                            chrome_browser.quit()
                            error = True

                        if 'Sorry' in chrome_browser.title:
                            print("bot detection url.........")
                            driver.add_expired_driver_proxy()
                            chrome_browser.quit()
                            error = True

                        if len(researcher_data) == 0:
                            chrome_browser.quit()
                            break

                        else:
                            if len(researcher_data) > 0:
                                self.append_researcher_data(researcher_data)
                                progress_current_url = chrome_browser.current_url

                                next_page = auth.check_next_page(chrome_browser)
                                print("Current", progress_current_url)
                                print("next", next_page)

                                if next_page == False:
                                    print("no next page moving to next organisation")
                                    chrome_browser.quit()
                                    break

                                else:
                                    active_page[0] = next_page
                                    progress_row = [org_domain, progress_current_url, next_page]

                                    self.append_progress_data(progress_row)
                                    error = False

                    except TimeoutException as ex:
                        print("Time out exeception", ex)
                        driver.add_expired_driver_proxy()
                        chrome_browser.quit()
                        error = True

                    except WebDriverException as ex:
                        driver.add_expired_driver_proxy()
                        chrome_browser.quit()
                        error = True

            except Exception as ex:
                print("something went wrong ", ex)
                traceback.print_exc()
                error = True

    def remove_all_before(self,last_uni, top_unis):
        try:
            for i in range(0, len(top_unis)):
                if (top_unis[i] == last_uni):
                    return top_unis[i:]

        except ValueError:
            print('border not present')


    def get_researcher(self):
        top_unis = self.get_top_uni_domain()
        progress_df = self.get_previous_progress()
        if len(progress_df.index) > 0:
            print("Picking up from the last progress")
            last_row_df = progress_df.iloc[-1:]

            last_uni = str(last_row_df['uni'].iloc[-1])
            next_url = str(last_row_df['current url'].iloc[-1])
            top_unis = self.remove_all_before(last_uni,top_unis)
            print("Total remaining uni", str(len(top_unis)))
            iteration_count = 0
            for org_domain in top_unis:
                print("Currently processing university name ", org_domain)
                if iteration_count == 0:
                    print(org_domain,next_url)
                    self.flip_pages(org_domain, next_url)

                else:
                    initial_url = "https://scholar.google.com/citations?hl=en&view_op=search_authors&mauthors=" + org_domain + "&btnG="
                    print(initial_url)
                    self.flip_pages(org_domain, initial_url)
                iteration_count = + 1

        else:
            print("Starting from the beginning")
            print("Total number of uni selected", len(top_unis))
            for org_domain in top_unis:
                print("Currently processing university name ", org_domain )
                initial_url = "https://scholar.google.com/citations?hl=en&view_op=search_authors&mauthors=" + org_domain + "&btnG="
                print(initial_url)
                self.flip_pages(org_domain, initial_url)


'''
researcher = Researcher("Jordan",35)
researcher.get_researcher()
'''
