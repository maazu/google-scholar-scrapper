import io
import re
import urllib3
import pdfplumber
import os
import inspect
import pandas as pd
from pathlib import Path
from selenium.common.exceptions import TimeoutException, WebDriverException, NoSuchElementException
from chrome_driver.chrome_driver import ChromeDriver
import traceback
from tqdm import tqdm
import time


class EmailExtractor:
    MAIL_REGEX_SECOND = re.compile(("([a-z0-9!#$%&*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                                    "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

    MAIL_REGEX = r'[\w.+-]+@[\w-]+\.[\w.-]+'
    FILE_EXTENSION = ".csv"
    REMOTE_LINKS_FILE = "-paper-host-links" + FILE_EXTENSION
    MAIL_DATA_FILE_NAME = "-extracted-mails" + FILE_EXTENSION
    MAIL_DATA_PROGRESS_FILE_NAME = "-extracted-mails-progress" + FILE_EXTENSION

    def __init__(self, country):
        self.country = country
        self.initial_path = str(Path(inspect.getfile(self.__class__)).absolute().parent.parent.parent)
        self.dir_path = self.initial_path + str(os.sep) + "scholar_data" + str(os.sep) + self.country + str(os.sep)
        self.remote_links_data_dir = self.dir_path + self.country + self.REMOTE_LINKS_FILE
        self.mail_data_dir = self.dir_path + self.country + self.MAIL_DATA_FILE_NAME
        self.mail_df = self.create_mail_df()
        self.driver = ChromeDriver()
        print(self.remote_links_data_dir)
        print(self.mail_data_dir)

    def create_mail_df(self):
        try:
            if not os.path.isfile(self.dir_path + self.country + self.MAIL_DATA_FILE_NAME):
                print("Creating mail data link File...")
                df = pd.DataFrame(columns=['researcher', 'paper links', 'extracted mails'])
                df.to_csv(self.dir_path + self.country + self.MAIL_DATA_FILE_NAME, index=False)
                print("mail data file created...", self.dir_path + self.country + self.MAIL_DATA_FILE_NAME)
                return df
            else:
                print("Previous Remote link File loaded..\nFile : ",
                      self.dir_path + self.country + self.MAIL_DATA_FILE_NAME)
                df = pd.read_csv(self.dir_path + self.country + self.MAIL_DATA_FILE_NAME)
                return df

        except Exception as e:
            print("Exception in created database")
            print(e)

    def read_remote_link_df(self):
        try:
            remote_link_df = pd.read_csv(self.remote_links_data_dir)
            return remote_link_df
        except Exception as e:
            print("Exception in remote df reading")
            traceback.print_exc()

            
    def append_mail_data(self, new_data_row):
        self.mail_df.loc[len(self.mail_df)] = new_data_row
        self.mail_df.to_csv(self.dir_path + self.country + self.MAIL_DATA_FILE_NAME, mode='a',
                            header=False, index=False)
        self.mail_df = pd.DataFrame(columns=self.mail_df.columns)

    def get_previous_progress(self):
        try:
            if os.path.isfile(self.dir_path + self.country + self.MAIL_DATA_PROGRESS_FILE_NAME):
                progress_df = pd.read_csv(self.dir_path + self.country + self.MAIL_DATA_PROGRESS_FILE_NAME)
                return progress_df
            else:
                progress_df = pd.DataFrame(columns=['last'])
                return progress_df
        except Exception as e:
             print(e)
             print("exception in progress")
             traceback.print_exc()

    def read_pdf(self, paper_host_link):
        try:
            http = urllib3.PoolManager()
            temp = io.BytesIO()
            temp.write(http.request("GET", paper_host_link).data)
            total_text = []
            with pdfplumber.open(temp) as pdf:
                total_pages = len(pdf.pages)
                for i in range(0, total_pages):
                    page_obj = pdf.pages[i]
                    page_text = page_obj.extract_text()
                    total_text.append(page_text)
            text = " ".join(total_text)
        except Exception as e:
            print(e)
            print(
                "somethig went wrong while extracting data from it could be either pdf not found on the url or "
                "resource has been moved.")
            text = "N/A"
        return text

    def pdf_mail_extractor(self, paper_host_link):
        text = self.read_pdf(paper_host_link)
        match = re.findall(self.MAIL_REGEX, text)
        if len(match) > 0:
            match = set(match)
            match = list(match)
        return match

    def get_emails(self, text):
        """Returns an iterator of matched emails found in string s."""
        # Removing lines that start with '//' because the regular expression
        # mistakenly matches patterns like 'http://foo@bar.com' as '//foo@bar.com'.
        return (email[0] for email in re.findall(self.MAIL_REGEX_SECOND, text) if not email[0].startswith('//'))

    def html_mail_extractor(self, text):
        try:
            emails = (list(set(self.get_emails(text))))
            MAIL_REGEX_SECOND = re.compile('[\w.+-]+@[\w-]+\.[\w.-]+')
            match = list(filter(MAIL_REGEX_SECOND.match, emails))
            if len(match) > 0:
                match = set(match)
                match = list(match)
            for mail in match:
                if not mail.endswith('png'):
                    emails.append(mail)
        except Exception as e:
            print("exception in email")
            traceback.print_exc()


        return emails

    def extract_emails(self):
        progress_df = self.get_previous_progress()
        remote_link_df = self.read_remote_link_df()
        total_remote_links = len(remote_link_df)
        chrome_browser = self.driver.chrome_driver_no_validation()
        error = False
        if len(progress_df.index) > 0:

            last_index = progress_df['last'].iloc[0]
            print(last_index)
        else:
            last_index = 0
        print("last progress for email links ==> ", last_index)

        for index, row in remote_link_df.iloc[last_index:].iterrows():
            if error:
                chrome_browser.quit()
                chrome_browser = self.driver.chrome_driver_no_validation()
                print("Chrome browser has been restarted....")
            try:
                profile_link = row['researcher']
                paper_host_link = row['host link']
                next_page = paper_host_link
                if next_page.endswith('.pdf'):
                    mail = self.pdf_mail_extractor(paper_host_link)
                else:
                    if paper_host_link.startswith('http://scholar'):
                        mail = "Google scholar link"
                    else:
                        chrome_browser.get(paper_host_link)
                        chrome_browser.set_page_load_timeout(30)
                        html_source_code = chrome_browser.page_source
                        mail = self.html_mail_extractor(html_source_code)

                if len(mail) == 0:
                    mail = "N/A"

                if len(mail) < 0:
                    mail = ""

                else:
                    if mail != "N/A":
                        mail = ' , '.join(str(e) for e in mail)

                data = [profile_link, paper_host_link, mail]

                self.append_mail_data(data)
                progress_df = pd.DataFrame(columns=['last'])
                progress_df.loc[len(progress_df)] = last_index
                progress_df.to_csv(self.dir_path + self.country + self.MAIL_DATA_PROGRESS_FILE_NAME, index=False)
                last_index = last_index + 1
                print(str(last_index) + "/" + str(len(remote_link_df)), paper_host_link, mail, flush=True)
                error = False

            except TimeoutException as ex:
                print("Time out exeception", ex)
                traceback.print_exc()
                error = True

            except WebDriverException as ex:
                print("Web Driver Issue ", ex)
                traceback.print_exc()
                error = True

        print("Closing the browser......")
        try:
            chrome_browser.quit()
            print("Browser has been closed ")
        except Exception as e:
            print("Browser has already been closed.")

