import argparse
from component.researcher.researcher import Researcher
from component.publication.publication import Publication
from component.publication.remote_link import PaperRemoteLink
from component.email.email_extractor import EmailExtractor
from util.internal_utils import create_country_directories

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=""" Google Scholar Scrapper """)
    parser.add_argument("--country", help="Country Name For Which you would like get the data")
    parser.add_argument("--mode", help="specify extraction mode: RE,RP, EM")
    args = parser.parse_args()
    create_country_directories()

    print("Execution flow "
          "\n--> first prepare profile links dataset"
          "\n--> second prepare papers reference link "
          "\n--> third prepare the paper host links"
          "\n--> finally extract email from the host link")

    COUNTRY = args.country
    MODE = args.mode
    if COUNTRY is None or COUNTRY == '':
        COUNTRY = input("Enter Country: ")

    if MODE is None or MODE == '':
        print("Chose Mode RE,RP, EM \n"
              "Step 1 --> 'RE' prepares the dataset of researchers from a particular country\n"
              "Step 2 --> 'RP' prepares the dataset of reference links which points to the host links.\n"
              "Step 3 --> 'RL' prepares the dataset of host link using the reference link dataset\n"
              "Step 4 --> 'EM' to extracted the emails from remote url from the prepared dataset.")
        MODE = input("Enter Mode: ")

    if MODE == "RE":
        # First Step Get All the Researcher
        researcher = Researcher(COUNTRY, 35)
        researcher.get_researcher()
        print("Researcher's Data extraction finished....")

    elif MODE =="RP":
        # Get Publications of Retrieved Researchers
        publications = Publication(COUNTRY)
        publications.extract_publications()
        print("Reference link extraction finished....")

    elif MODE == "RL":
        # Get Publications of remote links of retrieved publications
        remote_links = PaperRemoteLink(COUNTRY)
        remote_links.extract_remote_link()
        print("Remote link extraction finished....")

    elif MODE =="EM":
        email = EmailExtractor(COUNTRY)
        email.extract_emails()
        print("Email extraction finished....")

    else:
        print("Invalid Mode selection")
