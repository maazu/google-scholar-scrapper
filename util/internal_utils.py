import os
import sys
import datetime
from pathlib import Path
import pandas as pd


def get_current_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")


def create_country_directories():
    country_df = pd.read_csv(str(Path(os.getcwd()).absolute()) + str(os.sep) +"util" +str(os.sep)+ 'countries.csv')

    for index, row in country_df.iterrows():
        country = row['countries'].title()
        dir = os.listdir(str(Path(os.getcwd()).absolute()) + str(os.sep) +"scholar_data" + str(os.sep) )
        if country not in dir:
            os.mkdir(str(Path(os.getcwd()).absolute()) + str(os.sep) +"scholar_data" +str(os.sep) + country)



def get_researcher_directory(country):
    scholar_data_path = Path(os.getcwd()).absolute().parent  # each '.parent' goes one level up - vary as required
    scholar_data_path = str(scholar_data_path) + os.sep + "scholar_data" + os.sep + country


def get_scholar_data_path():
    scholar_data_path = Path(os.getcwd()).absolute().parent  # each '.parent' goes one level up - vary as required
    scholar_data_path = str(scholar_data_path) + os.sep + "scholar_data" + os.sep
    return scholar_data_path
