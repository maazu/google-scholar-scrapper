import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium import webdriver
from proxy.proxy import Proxy
import os, inspect


class ChromeDriver:

    def __init__(self):
        self.current_dir = os.path.dirname(inspect.getfile(self.__class__))
        self.options = webdriver.ChromeOptions()


    def get_chrome_driver_dir(self):
        chrome_driver_dir = self.current_dir + os.sep + "driver" + os.sep + "chromedriver.exe"
        return chrome_driver_dir

    def current_proxy(self):
        new_proxy = Proxy()
        return new_proxy

    def prepare_options(self):
        proxy_host = self.current_proxy()
        proxy = proxy_host.get_proxies()
        proxy_ip = proxy[0]
        self.options.add_argument("--log-level=3")
        self.options.add_argument('--deny-permission-prompts')
        self.options.add_argument("enable-automation")
        #self.options.add_argument('--headless')
        self.options.add_argument("--window-size=1920,1080")
        self.options.add_argument("--no-sandbox")
        self.options.add_argument("--disable-extensions")
        self.options.add_argument("--dns-prefetch-disable")
        self.options.add_argument("--disable-gpu")

        preferences = {"profile.default_content_setting_values.geolocation": 2}
        self.options.add_experimental_option("prefs", preferences)

        return self.options

    def check_current_url(self, chrome_driver, url):
        try:
            timeout = 35
            print("checking Test Url", url)
            chrome_driver.get(url)
            chrome_driver.set_page_load_timeout(timeout)
            time.sleep(5)
            if 'sorry' in chrome_driver.current_url:
                print("Bot detection Url")
                return False

            return True

        except TimeoutException as ex:
            return False

        except WebDriverException as ex:
            return False

    def validate_proxy_address(self, chrome_driver, url):
        validate_proxy = False
        while not validate_proxy:
            proxy_host = self.current_proxy()
            proxy = proxy_host.get_proxies()
            print("Total Proxy loaded ",str(len(proxy)))
            proxy_ip = proxy[0]
            result = self.check_current_url(chrome_driver, url)
            if result:
                print("New proxy ==>", proxy_ip)
                return True
            else:
                print("proxy added in the expired list",proxy_ip)
                proxy_host.add_expired_proxy(proxy_ip)
                return False

    def add_expired_driver_proxy(self):
        proxy_host = self.current_proxy()
        proxy = proxy_host.get_proxies()
        proxy_ip = proxy[0]
        proxy_host.add_expired_proxy(proxy_ip)
        print("proxy forcefully added into the expired list. Address:",proxy_ip)

    def chrome_driver_no_validation(self):
        driver_dir = self.get_chrome_driver_dir()
        #options = self.prepare_options()
        chrome_driver = webdriver.Chrome( executable_path=driver_dir)
        return chrome_driver

    def get_chrome_driver(self,url):
        driver_dir = self.get_chrome_driver_dir()
        options = self.prepare_options()
        chrome_driver = webdriver.Chrome(options=options, executable_path=driver_dir)
        if self.validate_proxy_address(chrome_driver,url):
            print("proxy working correctly")
            return chrome_driver
        else:
            print("re-instantiating the driver")
            chrome_driver.quit()
            self.get_chrome_driver(url)
