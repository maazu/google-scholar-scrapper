## Scholaper
_________________
![build](https://gitlab.com/gitlab-org/gitlab/badges/main/pipeline.svg?ignore_skipped=true)
![coverage](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage)

Scholar is a Python program which allow you to fetch researchers data from any country around the world,
This tool enables you to extract researcher data such as their citations, profile links, and recent publication.

### Dependencies
_________________
`pip3 install requirements.txt`
or 
`pip install requirements.txt`

### Usage
_________________
     main.py 

     Example 
     main.py  

Run the main.py and follow the instructions.
This script should be executed in four steps:
First run the main.py, enter the country and then mode 'RE'  to collect the researcher profiles from the specific country, once all the data is 
collected you will see the csv files in the scholar_data folder.
Once step one is finished rerun the script to get the reference links of te researcher's publication, select the 'RP' to execute this step, this 
will create another csv file in the same directory which contains the researcher's publications refernce links.
After finishing above steps, rerun the script use the mode 'RL' to get the remote links of the published papers.
Finally, rerun the script to extract the emails from the researchers remote links.



